package com.fortune.machine.rabbit.pg.rabitcompose

import androidx.compose.ui.graphics.Color

val LoaderOutlineColor = Color(0xFF925EF9)
val LoaderTrackColor = Color(0xFFFF5722)
val RulesTrackColor = Color(0xFFFDDD6E)
val WinColor = Color(0xFF24FF00)
