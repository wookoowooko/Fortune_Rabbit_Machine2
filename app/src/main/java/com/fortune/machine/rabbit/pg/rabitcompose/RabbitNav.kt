package com.fortune.machine.rabbit.pg.rabitcompose

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.fortune.machine.rabbit.pg.RabbitMainActivity
import com.fortune.machine.rabbit.pg.RabbitViewModel
import com.fortune.machine.rabbit.pg.rabbitUtils.RabbitLessViewModel
import com.fortune.machine.rabbit.pg.rabitcompose.RabbitNavRout.rabbitChoose
import com.fortune.machine.rabbit.pg.rabitcompose.RabbitNavRout.rabbitGame
import com.fortune.machine.rabbit.pg.rabitcompose.RabbitNavRout.rabbitLoad
import com.fortune.machine.rabbit.pg.rabitcompose.RabbitNavRout.rabbitMenu
import com.fortune.machine.rabbit.pg.rabitcompose.RabbitNavRout.rabbitPredExit
import com.fortune.machine.rabbit.pg.rabitcompose.RabbitNavRout.rabbitRules


object RabbitNavRout {
    val rabbitLoad = "rabbitLoad"
    val rabbitMenu = "rabbitMenu"
    val rabbitChoose = "rabbitChoose"
    val rabbitPredExit = "rabbitPredExit"
    val rabbitRules = "rabbitRules"
    val rabbitGame = "rabbitGame"
}

@Composable
fun RabbitNav(
    rabbitContextActiv: RabbitMainActivity,
    rabbitViewModel: RabbitViewModel,
    rabbitLessViewModel: RabbitLessViewModel,
) {

    val rabbitNav = rememberNavController()

    NavHost(navController = rabbitNav, startDestination = rabbitLoad) {
        composable(rabbitLoad) {
            RabbitLoad(rabbitLessViewModel, rabbitNav, rabbitContextActiv)
        }
        composable(rabbitMenu) {
            RabbitMenu(rabbitNav)
        }
        composable(rabbitChoose) {
            RabbitChoose(rabbitNav)
        }
        composable(rabbitPredExit) {
            RabbitPredExit(rabbitContextActiv, rabbitNav)
        }
        composable(rabbitRules) {
            RabbitRules(rabbitNav)
        }
        composable(rabbitGame) {
            RabbitGame(rabbitViewModel)
        }

    }

}