package com.fortune.machine.rabbit.pg

class RabbitGenerator {


    private val rabbitGeneratorListOfImages = listOf(
        R.drawable.ic_1,
        R.drawable.ic_2,
        R.drawable.ic_3,
        R.drawable.ic_4,
        R.drawable.ic_5,
        R.drawable.ic_6,
        R.drawable.ic_7,
    )

    val rabbitiToWordMap = mapOf(
        R.drawable.ic_1 to "карты",
        R.drawable.ic_2 to "ракеты",
        R.drawable.ic_3 to "монеты",
        R.drawable.ic_4 to "вайлд",
        R.drawable.ic_5 to "заяц",
        R.drawable.ic_6 to "пот",
        R.drawable.ic_7 to "морковь",
    )

    fun rabbitGenerateRandomListsFinal(count: Int): List<Int> {
        val shuffledImages = rabbitGeneratorListOfImages.shuffled()
        return shuffledImages.take(count)
    }

}