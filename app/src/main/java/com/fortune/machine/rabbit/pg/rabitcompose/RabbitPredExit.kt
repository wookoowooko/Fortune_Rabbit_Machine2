package com.fortune.machine.rabbit.pg.rabitcompose

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import androidx.navigation.NavHostController
import com.fortune.machine.rabbit.pg.R
import com.fortune.machine.rabbit.pg.RabbitMainActivity


@Composable
fun RabbitPredExit(rabbitContextActiv: RabbitMainActivity, rabbitNav: NavHostController) {

    Image(
        painter = painterResource(id = R.drawable.loadingsbeck),
        contentDescription = null,
        modifier = Modifier.fillMaxSize(),
        contentScale = ContentScale.Crop
    )

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Column(
            Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(0.5f)
                    .offset(y = 20.dp),
                contentAlignment = Alignment.BottomCenter
            ) {
                Column(
                    Modifier.fillMaxWidth(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Box(
                        modifier = Modifier
                            .offset(y = 40.dp)
                            .zIndex(2F),
                        contentAlignment = Alignment.Center
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.lenta),
                            contentDescription = null,
                            modifier = Modifier.fillMaxWidth(0.85f),
                            contentScale = ContentScale.Crop
                        )
                        Image(
                            painter = painterResource(id = R.drawable.exitpre),
                            contentDescription = null,
                            modifier = Modifier
                                .fillMaxWidth(0.2f)
                                .offset(y = (-20).dp),
                            contentScale = ContentScale.Crop
                        )
                    }


                    Box(
                        modifier = Modifier.fillMaxWidth(),
                        contentAlignment = Alignment.Center
                    ) {


                        Image(
                            painter = painterResource(id = R.drawable.bilboard),
                            contentDescription = null,
                            modifier = Modifier
                                .fillMaxWidth(0.85f)
                                .zIndex(1f),
                            contentScale = ContentScale.Crop
                        )

                        Column(
                            Modifier
                                .fillMaxWidth()
                                .zIndex(1F),
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Image(
                                painter = painterResource(id = R.drawable.ask),
                                contentDescription = null,
                                modifier = Modifier
                                    .fillMaxWidth(0.6f),
                                contentScale = ContentScale.Crop
                            )
                            Spacer(modifier = Modifier.height(20.dp))
                            Row(
                                modifier = Modifier.fillMaxWidth(0.6f),
                                verticalAlignment = Alignment.CenterVertically,
                                horizontalArrangement = Arrangement.SpaceAround
                            ) {
                                Box(
                                    modifier = Modifier,
                                    contentAlignment = Alignment.Center
                                ) {
                                    Image(
                                        painter = painterResource(id = R.drawable.yesbyrn),
                                        contentDescription = null,
                                        contentScale = ContentScale.Crop,
                                        modifier = Modifier
                                            .fillMaxWidth(0.5f)
                                            .scale(0.8f)
                                            .clickable {
                                                rabbitContextActiv.finishAffinity()
                                            }
                                    )
                                    Image(
                                        painter = painterResource(id = R.drawable.ye),
                                        contentDescription = null,
                                        modifier = Modifier
                                            .scale(1.8f)
                                    )
                                }


                                Box(
                                    modifier = Modifier,
                                    contentAlignment = Alignment.Center
                                ) {
                                    Image(
                                        painter = painterResource(id = R.drawable.yesbyrn),
                                        contentDescription = null,
                                        contentScale = ContentScale.Crop,
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .clickable { rabbitNav.navigate(RabbitNavRout.rabbitChoose) }
                                            .scale(0.8f)
                                    )
                                    Image(
                                        painter = painterResource(id = R.drawable.no),
                                        contentDescription = null,
                                        modifier = Modifier
                                            .scale(1.8f)
                                    )
                                }
                            }
                        }
                    }


                }

            }
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(),
                contentAlignment = Alignment.BottomCenter
            ) {
                Image(
                    painter = painterResource(id = R.drawable.rabbit_pred_exit),
                    contentDescription = null,
                    modifier = Modifier.fillMaxSize()
                )
            }
        }
    }

}