package com.fortune.machine.rabbit.pg

data class RabbitRulesData(
    val rabbitRule: String,
)

object RabbitRulesDTO {

    val rabbitRulesList = listOf(
        RabbitRulesData(

            rabbitRule = "1. Set your bid: Adjust your bid by selecting the desired amount"
        ),

        RabbitRulesData(
            rabbitRule = "2. Check your balance: Make sure you have enough virtual points in your balance"
        ),
        RabbitRulesData(

            rabbitRule = "3. Press \"Spin\": Press the \"Spin\" button to spin the reels of the slot machine."
        ),
        RabbitRulesData(

            rabbitRule = "4.Wait for the result: Watch the reels start spinning and then randomly stop to form a combination of symbols."
        ),
        RabbitRulesData(

            rabbitRule = "5. Win or lose: If the symbols form a winning combination, you will receive a payout that will increase your balance. If not, your bet will be deducted from your balance."
        ),
        RabbitRulesData(
            rabbitRule = "6. Adjust your bid: You can change your bid amount at any time."
        ),
        RabbitRulesData(

            rabbitRule = "7. Manage your balance: Monitor your virtual credit balance to avoid overspending."
        ),
    )
}
